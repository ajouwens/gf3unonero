GF3UnoNero - 24 Hour Single Hand Watch
======================================

Description
-----------

This watch face shows the time on a 24 hour scale with a single hand. Based on the Botta Uno 24 Neo.
It also shows the date, with a toggle to show the minutes when the watch is in "high power mode".
--------------
Attention: low- and high power mode is a feature which has some quirks, therefor the toggle between date and minutes may work sub-optimal.
--------------
Please, leave your comments, bug reports and ideas on this forum:
https://forums.garmin.com/showthread.php?265723-Watchfaces-GF3-
--------------
Version 1.1.1:
- Changed the toggle time to 2 seconds.
- When entering low power mode, it will go back to show the date.
Version 1.1:
- Changed the date font, alignment of the previous number font was not ok.
- In 'high power mode' the watch face shows after 3 seconds for 1 second the current minute in a different color.
(Thanx berglauf1234 for your suggestion)
--------------
See:
- GF3UnoLuce for the lighter version
- GF3UnoScuro for the darker version

